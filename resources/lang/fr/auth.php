<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Ces informations d\'identification ne correspondent pas.',
    'throttle' => 'Trop de tentatives de connexion. Réessayez dans :seconds secondes.',

    // Activation items
    'sentEmail'         => 'Nous avons envoyé un e-mail à :email.',
    'clickInEmail'      => 'Veuillez cliquer sur le lien pour activer votre compte.',
    'anEmailWasSent'    => 'Un e-mail a été envoyé à :email le :date.',
    'clickHereResend'   => 'Cliquez ici pour renvoyer l\'e-mail.',
    'successActivated'  => 'Succès, votre compte a été activé.',
    'unsuccessful'      => 'Votre compte n\'a pas pu être activé. Veuillez réessayer.',
    'notCreated'        => 'Votre compte n\'a pas pu être créé; Veuillez réessayer.',
    'tooManyEmails'     => 'Plusieurs d\'e-mails d\'activation ont été envoyés à :email. <br />Veuillez réessayer dans <span class="label label-danger">:hours heures</span>.',
    'regThanks'         => 'Merci de votre inscription, ',
    'invalidToken'      => 'Jeton d\'activation invalide. ',
    'activationSent'    => 'E-mail d\'activation envoyé. ',
    'alreadyActivated'  => 'Déjà activé. ',

    // Labels
    'whoops'            => 'Oups! ',
    'someProblems'      => 'Il y a eu quelques problèmes avec votre saisie.',
    'email'             => 'Adresse e-mail',
    'password'          => 'Mot de Passe',
    'rememberMe'        => ' Se souvenir de moi',
    'login'             => 'Se connecter',
    'forgot'            => 'Mot de passe oublié?',
    'forgot_message'    => 'Problèmes de mot de passe?',
    'name'              => 'Nom d\'utilisateur',
    'first_name'        => 'Prénom(s)',
    'last_name'         => 'Nom',
    'confirmPassword'   => 'Confirmez le mot de passe',
    'register'          => 'S\'inscrire',

    // Placeholders
    'ph_name'           => 'Entrez votre Nom d\'utilisateur',
    'ph_email'          => 'Entrez votre Adresse e-mail',
    'ph_firstname'      => 'Entrez votre/vos Prénom(s)',
    'ph_lastname'       => 'Entrez votre Nom',
    'ph_password'       => 'Entrez votre Mot de Passe',
    'ph_password_conf'  => 'Saisissez à nouveau votre mot de passe',

    // User flash messages
    'sendResetLink'     => 'Envoyer le lien ne reinitialisation du mot de passe',
    'resetPassword'     => 'Reinitialiser le mot de passe',
    'loggedIn'          => 'Vous êtes authentifié!',

    // email links
    'pleaseActivate'    => 'Veuillez activer votre compte.',
    'clickHereReset'    => 'Cliquez ici pour réinitialiser votre mot de passe: ',
    'clickHereActivate' => 'Cliquez ici pour activer votre compte: ',

    // Validators
    'userNameTaken'     => 'Le nom d\'utilisateur est pris',
    'userNameRequired'  => 'Nom d\'utilisateur est requis',
    'fNameRequired'     => 'Le Prénom est requis',
    'lNameRequired'     => 'Le Nom est requis',
    'emailRequired'     => 'L\'e-mail est requis',
    'emailInvalid'      => 'L\'e-mail est invalide',
    'passwordRequired'  => 'Le mot de passe est requis',
    'PasswordMin'       => 'Le mot de passe doit comporter au moins 6 caractères',
    'PasswordMax'       => 'La longueur maximale du mot de passe est de 20 caractères',
    'contactMax'        => 'La longueur maximale de contact est de 23 caractères',
    'captchaRequire'    => 'Captcha est requis',
    'CaptchaWrong'      => 'Mauvais captcha, veuillez réessayer.',
    'roleRequired'      => 'Le rôle utilisateur est requis.',

];
