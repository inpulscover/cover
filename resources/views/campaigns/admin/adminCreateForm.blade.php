@extends('layouts.backendAdmin')

@section('title')
    New Campaign
@endsection

@section('page_linked_css')

@endsection

@section('content')
    <div class="col-md-10 offset-1">
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">{{Lang::get('campaign.title.create')}}</div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                    <a class="config" data-toggle="modal" href="#grid-config"></a>
                    <a class="reload" href="javascript:;"></a>
                    <a class="remove" href="javascript:;"></a>
                </div>
            </div>
            <div class="card-block">
                <div class="row p-t-20">
                    <form id="userCreateCampaignForm" role="form" enctype="multipart/form-data" autocomplete="off" style="width:100%;" method="post" action="{{route('campaignSave')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} form-group-default required" aria-required="true">
                                    <label>Titre</label>
                                    <input type="text" class="form-control" name="title" placeholder="Enter campaign title ..." value="{{old('title')}}" aria-required="true" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('dateDebut') ? 'has-error' : '' }} form-group-default input-group required">
                                    <div class="form-input-group">
                                        <label>Debut Campagne</label>
                                        <input type="text" name="dateDebut" class="form-control" placeholder="Pick a date" value="{{old('dateDebut')}}" id="datepicker-component2" required>
                                        @if ($errors->has('dateDebut'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dateDebut') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('dateFin') ? 'has-error' : '' }} form-group-default input-group required">
                                    <div class="form-input-group">
                                        <label>Fin Campagne</label>
                                        <input type="text" name="dateFin" class="form-control" placeholder="Pick a date" value="{{old('dateFin')}}" id="datepicker-component2" required>
                                        @if ($errors->has('dateFin'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dateFin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="form-group form-group-default input-group">
                                    <div class="form-input-group">
                                        <label>Description</label>
                                        <textarea name="description" class="form-control" placeholder="Entrer une description ..." style="height: 150px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('imgBack') ? 'has-error' : '' }} form-group-default input-group required">
                                    <div class="form-input-group">
                                        <label>Image D'arriere Plan</label>
                                        <input name="imgBack" type="file" class="form-control" aria-required="true" />
                                        @if ($errors->has('imgBack'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('imgBack') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('imgTag') ? 'has-error' : '' }} form-group-default input-group required">
                                    <div class="form-input-group">
                                        <label>Image de Tag</label>
                                        <input name="imgTag" type="file" class="form-control" aria-required="true" />
                                        @if ($errors->has('imgTag'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('imgTag') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group form-group-default input-group p-b-10">
                                    <div class="form-input-group m-l-10">
                                        <label class="no-padding">Campaign Owner</label>
                                        <select name="userId" class="form-control" data-init-plugin="select2" style="width: 75%;">
                                            <option value="self">Myself</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->name}}">{{$user->first_name.' '.$user->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if(config('settings.reCaptchStatus'))
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <div class="g-recaptcha" data-sitekey="{{ config('settings.RE_CAP_SITE') }}"></div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="p-t-15">
                            <div class="clearfix"></div>
                            <button class="btn btn-primary" type="submit">Create a new campaign</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection