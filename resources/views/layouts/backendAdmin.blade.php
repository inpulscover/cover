<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | Admin - @yield('title')</title>

    {{-- Fonts --}}
    @yield('template_linked_fonts')

    {{-- Styles --}}
    <link rel="apple-touch-icon" href="{{asset('backend/admin/pages/ico/60.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('backend/admin/pages/ico/76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('backend/admin/pages/ico/120.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('backend/admin/pages/ico/152.png')}}">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- BEGIN PLUGINS -->
    <link href="{{asset('backend/admin/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/admin/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/admin/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/admin/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('backend/admin/assets/plugins/summernote/css/summernote.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('backend/admin/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('backend/admin/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('backend/admin/assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/admin/assets/plugins/mapplic/css/mapplic.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/assets/plugins/rickshaw/rickshaw.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/admin/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{asset('backend/admin/pages/css/themes/corporate.css')}}" rel="stylesheet" type="text/css" />

    @yield('page_linked_css')

    <style type="text/css">

        @if (Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0))
            .user-avatar-nav {
                background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
                background-size: auto 100%;
            }
        @endif

        a:hover, a:focus, a:active{
            text-decoration: none !important;
        }
    </style>

</head>
<body class="fixed-header dashboard menu-pin menu-behind">

@include('.partials.admin.sidebar-admin')

<!-- START PAGE-CONTAINER -->
<div class="page-container ">

    @include('.partials.admin.nav-admin')

    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid padding-25 sm-padding-10">

                @yield('content');

            </div>
            <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->


        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid  container-fixed-lg footer">
            <!-- START COPYRIGHT -->
            <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text">Copyright &copy; 2017 </span>
                    <span class="font-montserrat">REVOX</span>.
                    <span class="hint-text">All rights reserved. </span>
                    <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> <span class="muted">|</span> <a href="#" class="m-l-10">Privacy Policy</a></span>
                </p>
                <p class="small no-margin pull-right sm-pull-reset">Hand-crafted<span class="hint-text">&amp; made with Love</span></p>
                <div class="clearfix"></div>
            </div>
            <!-- END COPYRIGHT -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->

@include('.partials.admin.quickview-admin')

@include('.partials.admin.search-admin')

<!-- BEGIN VENDOR JS -->
<script src="{{asset('backend/admin/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/tether/js/tether.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
<script src="{{asset('backend/admin/assets/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/classie/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/lib/d3.v3.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/nv.d3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/src/utils.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/src/tooltip.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/src/interactiveLayer.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/src/models/axis.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/src/models/line.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/nvd3/src/models/lineWithFocusChart.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/mapplic/js/hammer.js')}}"></script>
<script src="{{asset('backend/admin/assets/plugins/mapplic/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('backend/admin/assets/plugins/mapplic/js/mapplic.js')}}"></script>
<script src="{{asset('backend/admin/assets/plugins/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{asset('backend/admin/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/plugins/skycons/skycons.js')}}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{asset('backend/admin/pages/js/pages.min.js')}}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{asset('backend/admin/assets/js/dashboard.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/js/form_elements.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/admin/assets/js/scripts.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->

<script src="{{ mix('/js/app.js') }}"></script>

@yield('footer_scripts')

</body>
</html>