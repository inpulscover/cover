@extends('layouts.frontendUser')

@section('title')
    {{Lang::get('auth.register')}}
@endsection

@section('page_linked_css')

    <style>
        .form-horizontal .form-group .control-label{
            opacity: 1;
        }
    </style>
@endsection

@section('content')
    <section class="p-b-65 p-t-90 full-height xs-full-width" style="background-color: #ddd;">
        <div class="container full-height">
            <div class="row full-height">
                <div class="container p-t-35">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3>{{Lang::get('titles.register')}}</h3></div>
                                <div class="panel-body">

                                    {!! Form::open(['route' => 'register', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST'] ) !!}

                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-sm-4 control-label">{{Lang::get('auth.email')}}</label>
                                        <div class="col-sm-6">
                                            {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => Lang::get('auth.ph_email'), 'required']) !!}
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-sm-4 control-label">{{Lang::get('auth.password')}}</label>
                                        <div class="col-sm-6">
                                            {!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => Lang::get('auth.ph_password'), 'required']) !!}
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm" class="col-sm-4 control-label">{{Lang::get('auth.confirmPassword')}}</label>
                                        <div class="col-sm-6">
                                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password-confirm', 'placeholder' => Lang::get('auth.ph_password_conf'), 'required']) !!}
                                        </div>
                                    </div>
                                    @if(config('settings.reCaptchStatus'))
                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-4">
                                                <div class="g-recaptcha" data-sitekey="{{ config('settings.RE_CAP_SITE') }}"></div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group margin-bottom-2">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{Lang::get('auth.register')}}
                                            </button>
                                        </div>
                                    </div>

                                    @if(config('settings.socialLoginStatus'))
                                        <p class="text-center margin-bottom-2">
                                            Or Use Social Logins to Register
                                        </p>

                                        @include('partials.socials')
                                    @endif

                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_scripts')

    <script src='https://www.google.com/recaptcha/api.js'></script>

@endsection