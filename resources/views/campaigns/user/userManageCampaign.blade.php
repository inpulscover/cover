@extends('layouts.backendUser')

@section('title')
    Manage Campaign
@endsection

@section('page_linked_css')

@endsection

@section('content')
    <div class="col-md-12 no-padding">
        <h2 class="inline">{{Lang::get('campaign.manage')}}</h2>
        <a class="btn btn-primary hidden-xs pull-right m-t-10" href="{{route('create.campaign')}}">{{Lang::get('campaign.new')}}</a>
        <a class="btn btn-primary hidden-md-up pull-right m-t-10" href="{{route('create.campaign')}}"><i class="fa fa-plus fa-fw"></i></a>
    </div>
    <table class="table table-responsive-block demo-table-search tableWithSearch" id="userDataTableCampaign" role="grid" aria-describedby="tableWithSearch_info">
        <thead>
            <tr role="row">
                <th style="width: 180px;">{{Lang::get('titles.dataTbTitle')}}</th>
                <th style="width: 200px;">{{Lang::get('titles.dataTbDesc')}}</th>
                <th style="width: 85px;">{{Lang::get('titles.dataTbStatus')}}</th>
                <th style="width: 120px;">{{Lang::get('titles.dataTbState')}}</th>
                <th style="width: 140px;">{{Lang::get('titles.dataTbAction')}}</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{asset('backend\user\assets\js\datatables.js')}}"></script>
@endsection