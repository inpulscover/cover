@extends('layouts.backendUser')

@section('title')
    {{Lang::get('campaign.show')}}
@endsection

@section('page_linked_css')

@endsection

@section('content')
    <div class="col-md-10" style="margin: auto;">
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title"><h4>{{Lang::get('campaign.show')}}</h4></div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                    <a class="config" data-toggle="modal" href="#grid-config"></a>
                    <a class="reload" href="javascript:;"></a>
                    <a class="remove" href="javascript:;"></a>
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-6">
                        <h6>{{Lang::get('campaign.mediaInfo')}}</h6>
                        <div class="form-group form-group-default" aria-required="true">
                            <label>{{Lang::get('campaign.imgBackground')}}</label>
                            <div>
                                <img src="{{asset('uploads/campaigns/'.$campaign->imgBackground)}}" width="75%" />
                            </div>
                        </div>
                        <div class="form-group form-group-default" aria-required="true">
                            <label>{{Lang::get('campaign.imgTag')}}</label>
                            <div>
                                <img src="{{asset('uploads/campaigns/'.$campaign->imgTag)}}" width="45%" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h6>{{Lang::get('campaign.basicInfo')}}</h6>
                        <div class="form-group form-group-default" aria-required="true">
                            <label>{{Lang::get('campaign.title')}}</label>
                            <div>{{ucfirst($campaign->title)}}</div>
                        </div>
                        <div class="form-group form-group-default" aria-required="true">
                            <label>{{Lang::get('campaign.dateDebut')}}</label>
                            <div>{{$campaign->dateDebut}}</div>
                        </div>
                        <div class="form-group form-group-default" aria-required="true">
                            <label>{{Lang::get('campaign.dateFin')}}</label>
                            <div>{{$campaign->dateFin}}</div>
                        </div>
                        <div class="form-group form-group-default" aria-required="true">
                            <label>{{Lang::get('campaign.description')}}</label>
                            <div>{{ucfirst($campaign->description)}}</div>
                        </div>
                        <div class="p-t-15">
                            <div class="clearfix"></div>
                            <a href="{{route('edit.campaign', ['idCampaign' => $campaign->id])}}" class="btn btn-primary pull-right m-l-5 m-r-5">{{Lang::get('campaign.editText')}}</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')

@endsection