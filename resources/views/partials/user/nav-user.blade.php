<div class="header p-r-0 bg-primary">
    <div class="header-inner header-md-height">
        <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu text-white" data-toggle="horizontal-menu"></a>
        <div class="">
            <div class="brand inline no-border hidden-xs-down">
                <img src="{{asset('backend/user/assets/img/logo_white.png')}}" alt="logo" data-src="{{asset('backend/user/assets/img/logo_white.png')}}" data-src-retina="{{asset('backend/user/assets/img/logo_white_2x.png')}}" width="78" height="22">
            </div>
        </div>
        <div class="d-flex align-items-center">
            <!-- START User Info-->
            <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down text-white">
                <span class="semi-bold">{{Auth::user()->name}}</span>
            </div>
            <div class="dropdown pull-right">
                <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="thumbnail-wrapper d32 circular inline sm-m-r-5">
                        @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                            <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->name }}" class="user-avatar-nav">
                        @else
                            <div class="user-avatar-nav"></div>
                        @endif
                    </span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                    <li class="dropdown-item">
                        <a href="{{route('public.home')}}" class="no-padding"><i class="pg-home"></i> {{trans('titles.dashboard')}}</a>
                    </li>
                    <!-- liclass="dropdown-item ">
                        <a href="/profile/{{Auth::user()->name}}" class="no-padding"><i class="pg-cupboard"></i> {{trans('titles.profile')}}</a>
                        <{!! HTML::link(url('/profile/'.Auth::user()->name), trans('titles.profile'), ['class'=>'no-padding']) !!}>
                    </li-->
                    <li class="dropdown-item">
                        <a class="clearfix no-padding" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {!! trans('titles.logout') !!}
                            <span class="pull-right"><i class="pg-power"></i></span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
            <!-- END User Info
            <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a>-->
        </div>
    </div>
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
                </a>
                <ul>
                    <li {{ Request::is('dashboard', 'profile/' . Auth::user()->name, 'profile/' . Auth::user()->name . '/edit') ? 'class=active' : null }}>
                        <a href="{{route('public.home')}}">{{Lang::get('titles.dashboard')}}</a>
                    </li>
                    <li {{ Request::is('campaign', 'campaign/create', 'campaign/manage') ? 'class=active' : null }}>
                        <a href="javascript:;"><span class="title">{{Lang::get('titles.campaign')}}</span>
                            <span class=" arrow"></span></a>
                            <ul class="">
                                <li class="">
                                    <a href="{{route('create.campaign')}}">{{Lang::get('campaign.new')}}</a>
                                </li>
                                <li class="">
                                    <a href="{{route('manage.campaign')}}">{{Lang::get('campaign.manage')}}</a>
                                </li>
                            </ul>
                    </li>
                </ul>
                <a href="#" class="search-link d-flex justify-content-between align-items-center hidden-lg-up" data-toggle="search">Tap here to search <i class="pg-search float-right"></i></a>
            </div>
        </div>
    </div>
</div>