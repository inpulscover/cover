<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'campaigns';

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attribute that mass assignable
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'dateDebut', 'dateFin', 'imgTag',
                            'imgBackground', 'code', 'user_id'];

    // Campaigns Users
    public function users(){

        return $this->belongsTo('App\Models\User');

    }

    // Campaigns Campaigns_Users
    public function campaignUsers(){

        return $this->hasMany('App\Models\CampaignUser');

    }


}
