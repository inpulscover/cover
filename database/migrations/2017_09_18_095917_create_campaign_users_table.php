<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_users', function (Blueprint $table) {

            $table->engine = 'MyISAM';

            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('sex');
            $table->string('country');
            $table->string('identifiant');
            $table->string('imgProfile');

            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on("providers");

            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_users');
    }
}
