@extends('layouts.frontendUser')

@section('title')
	{{ Lang::get('titles.activation') }}
@endsection

@section('content')
	<section class="p-b-65 p-t-90 full-height xs-full-width" style="background-color: #ddd;">
		<div class="container full-height">
			<div class="row full-height">
				<div class="container p-t-90">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="panel panel-default">
								<div class="panel-heading">{{ Lang::get('titles.activation') }}</div>
								<div class="panel-body">
									<p>{{ Lang::get('auth.regThanks') }}</p>
									<p>{{ Lang::get('auth.anEmailWasSent',['email' => $email, 'date' => $date ] ) }}</p>
									<p>{{ Lang::get('auth.clickInEmail') }}</p>
									<p><a href='/activation' class="btn btn-primary">{{ Lang::get('auth.clickHereResend') }}</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection