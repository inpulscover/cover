<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\User;
use App\Traits\CaptchaTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Intervention\Image\Facades\Image;
use Validator;
use Yajra\DataTables\Facades\DataTables;

class CampaignController extends Controller
{

    use CaptchaTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data){

        if(config('settings.reCaptchStatus')) {
            $data['captcha'] = $this->captchaCheck();

            if (!config('settings.reCaptchStatus')) {
                $data['captcha'] = true;
            }

            $captchaRule = "required|min:1";
        }
        else{
            $captchaRule = '';
        }

        $today = Carbon::today()->format('m/d/Y');


        return Validator::make($data,
            [
                'title'                 => 'required|max:255',
                'dateDebut'             => "required|date_format:m/d/Y|after_or_equal:$today",
                'dateFin'               => "required|date_format:m/d/Y|after:dateDebut",
                'description'           => '',
                'imgBack'               => 'file|mimes:jpeg,jpg,bmp,png|between:20,1024|dimensions:min_width=1300,min_height=700',
                'imgTag'                => 'file|mimes:png|between:20,1024',
                'captcha'               => "$captchaRule",
            ],
            [
                'title.required'                => trans('campaign.titleRequired'),
                'title.max'                     => trans('campaign.titleMax'),
                'dateDebut.required'            => trans('campaign.dateDebutRequired'),
                'dateDebut.date_format'         => trans('campaign.dateDebutFormat'),
                'dateDebut.after_or_equal'      => trans('campaign.dateDebutMin'),
                'dateFin.required'              => trans('campaign.dateFinRequired'),
                'dateFin.date_format'           => trans('campaign.dateFinFormat'),
                'dateFin.after'                 => trans('campaign.dateFinMin'),
                'imgBack.required'              => trans('campaign.imgBackRequired'),
                'imgTag.required'               => trans('campaign.imgTagRequired'),
                'imgBack.file'                  => trans('campaign.uploadFile'),
                'imgBack.mimes'                 => trans('campaign.imageMIME'),
                'imgBack.between'               => trans('campaign.imageSize'),
                'imgBack.dimensions'            => trans('campaign.imageDimensions'),
                'imgTag.file'                   => trans('campaign.imageFile'),
                'imgTag.mimes'                  => trans('campaign.imageMIME'),
                'imgTag.between'                => trans('campaign.imageSize'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min'                   => trans('auth.CaptchaWrong'),
            ]
        );

    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function validatorEdit(array $data){

        if(config('settings.reCaptchStatus')) {
            $data['captcha'] = $this->captchaCheck();

            if (!config('settings.reCaptchStatus')) {
                $data['captcha'] = true;
            }

            $captchaRule = "required|min:1";
        }
        else{
            $captchaRule = '';
        }


        return Validator::make($data,
            [
                'title'                 => 'required|max:255',
                'dateDebut'             => "date_format:m/d/Y",
                'dateFin'               => "date_format:m/d/Y",
                'description'           => '',
                'imgBack'               => 'file|mimes:jpeg,jpg,bmp,png|between:20,1024|dimensions:min_width=1300,min_height=700',
                'imgTag'                => 'file|mimes:png|between:20,1024',
                'captcha'               => "$captchaRule",
            ],
            [
                'title.required'                => trans('campaign.titleRequired'),
                'title.max'                     => trans('campaign.titleMax'),
                'dateDebut.date_format'         => trans('campaign.dateDebutFormat'),
                'dateFin.date_format'           => trans('campaign.dateFinFormat'),
                'imgBack.required'              => trans('campaign.imgBackRequired'),
                'imgTag.required'               => trans('campaign.imgTagRequired'),
                'imgBack.file'                  => trans('campaign.uploadFile'),
                'imgBack.mimes'                 => trans('campaign.imageMIME'),
                'imgBack.between'               => trans('campaign.imageSize'),
                'imgBack.dimensions'            => trans('campaign.imageDimensions'),
                'imgTag.file'                   => trans('campaign.imageFile'),
                'imgTag.mimes'                  => trans('campaign.imageMIME'),
                'imgTag.between'                => trans('campaign.imageSize'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min'                   => trans('auth.CaptchaWrong'),
            ]
        );

    }

    /**
     * Show Campaign create form
     * @return View
     */
    public function create(){

        $user = Auth::user();
        $owners = User::all();

        if($user->isAdmin()){

            return view('campaigns.admin.adminCreateForm')->with(['users'=>$owners]);

        }

        return view('campaigns.user.userCreateForm');

    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function save(Request $request){

        $data = $request->all();
        $validator = $this->validator($data);

        if($validator->fails()){

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(isset($data['userId']) && $data['userId'] != 'self'){

            $user = User::where('name', '=', $data['userId'])->first();

        }else{

            $user = Auth::user();

        }

        $campaign = New Campaign();

        $campaign->title          = $data['title'];
        $campaign->dateDebut      = Carbon::parse($data['dateDebut'])->toDateString();
        $campaign->dateFin        = Carbon::parse($data['dateFin'])->toDateString();
        $campaign->description    = $data['description'];

        if(Input::file('imgBack')){
            $imgBack = Input::file('imgBack');
            $filename = time() . '.' . $imgBack->getClientOriginalExtension();
            $path = public_path('/uploads/campaigns/'.$filename);
            Image::make($imgBack->getRealPAth())->save($path);
            $campaign->imgBackground = $filename;
        }
        else{
            $campaign->imgBackground = 'campaign_background_img.jpg';
        }

        if(Input::file('imgTag')){
            $imgTag = Input::file('imgTag');
            $filename = time() . '.' . $imgTag->getClientOriginalExtension();
            $path = public_path('/uploads/campaigns/'.$filename);
            Image::make($imgTag->getRealPAth())->save($path);
            $campaign->imgTag = $filename;
        }else{
            $campaign->imgTag = 'campaign_tag_img.png';
        }

        $campaign->code = str_random(60);
        $campaign->user_id = $user->id;

        if(config('settings.adminCampaignValidation')){

            $campaign->status = 'waiting';
            $campaign->state = 'undefined';

        }else{

            $campaign->status = 'validated';
            $campaign->state = 'running';

        }

        if($campaign->save()){

            return redirect()->route('manage.campaign')->with('response', ['success'=>true, 'msg'=> Lang::get('campaign.successCreateNotif')]);

        }
        else{

            return redirect()->back()->with('response', ['success'=>false, 'msg'=> Lang::get('campaign.failCreateNotif')]);

        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manage(){

        if(Auth::user()->isAdmin()){

            return view('campaigns.admin.adminManageCampaign');

        }

        return view('campaigns.user.userManageCampaign');

    }

    /**
     * @return mixed
     */
    public function getUserDataTableCampaign(){

        if(Auth::user()){

            $user = Auth::user();

            $campaign = DB::table('campaigns')
                ->select('id', 'title', 'description', 'dateDebut as date_debut', 'dateFin as date_fin', 'state', 'status')
                ->where('user_id', $user->id)
                ->orderBy('id', 'desc');
            return DataTables::of($campaign)
                ->addColumn('status', function($campaign){
                    if($campaign->status == 'waiting'){
                        return "<a href='javascript:;' class='btn btn-tag' style='background-color: #1f3953; color: #fff !important;'>".ucfirst($campaign->status)."</a>";
                    }else if($campaign->status == 'validated'){
                        return "<a href='javascript:;' class='btn btn-tag' style='background-color: #006045; color: #fff !important;'>".ucfirst($campaign->status)."</a>";
                    }else if($campaign->status == 'rejected'){
                        return "<a href='javascript:;' class='btn btn-tag' style='background-color: #de0c26; color: #fff !important;'>".ucfirst($campaign->status)."</a>";
                    }
                })
                ->addColumn('state', function($campaign){
                    if($campaign->status == 'validated'){

                        if($campaign->state == 'undefined' or $campaign->state == 'completed'){
                            return "<p><span style='color: darkgrey;'><strong>".strtoupper($campaign->state)."</strong></span></p>
                                <button class='btn btn-success start-campaign disabled' title='Start this campaign'><i class='fa fa-play fs-14'></i></button>
                                <button class='btn btn-danger stop-campaign disabled' title='Stop this campaign'><i class='fa fa-stop fs-14'></i></button>";
                        }else if($campaign->state == 'running'){
                            return "<p><span style='color: darkgreen;'><strong>".strtoupper($campaign->state)."</strong></span></p>
                                <button class='btn btn-success start-campaign disabled' title='Start this campaign'><i class='fa fa-play fs-14'></i></button>
                                <button class='btn btn-danger stop-campaign' data-toggle='modal' data-target='#modalStopCampaign_".$campaign->id."'  title='Stop this campaign'><i class='fa fa-stop fs-14'></i></button>
                                <!-- Modal -->
                                <div class='modal fade slide-up disable-scroll' id='modalStopCampaign_".$campaign->id."'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content-wrapper'>
                                        <div class='modal-content'>
                                            <div class='modal-header clearfix text-left'>
                                                <h3>".Lang::get('campaign.confirmText')."</h3>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>
                                                      <i class='pg-close fs-14'></i>
                                                </button>
                                            </div>
                                            <div class='modal-body m-t-20'>
                                                <h4 class='no-margin p-b-10'>".Lang::get('campaign.confirmStop')."</h4>
                                                <button type='button' class='btn btn-danger pull-right m-l-5 m-r-5' data-dismiss='modal'>".Lang::get('campaign.cancelText')."</button>
                                                <button type='button' class='btn btn-primary confirm-stop-campaign pull-right m-l-5 m-r-5' data-id='".$campaign->id."'>".Lang::get('campaign.confirmTextAction')."</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                </div>
                                <!-- /.modal-dialog -->";
                        }else if($campaign->state == 'stopped'){
                            return "<p><span style='color: darkred;'><strong>".strtoupper($campaign->state)."</strong></span></p>
                                <button class='btn btn-success start-campaign' data-toggle='modal' data-target='#modalStartCampaign_".$campaign->id."' title='Start this campaign'><i class='fa fa-play fs-14'></i></button>
                                <button class='btn btn-danger stop-campaign disabled' title='Stop this campaign'><i class='fa fa-stop fs-14'></i></button>
                                <!-- Modal -->
                                <div class='modal fade slide-up disable-scroll' id='modalStartCampaign_".$campaign->id."'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content-wrapper'>
                                            <div class='modal-content'>
                                                <div class='modal-header clearfix text-left'>
                                                    <h3>".Lang::get('campaign.confirmText')."</h3>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>
                                                      <i class='pg-close fs-14'></i>
                                                    </button>
                                                </div>
                                                <div class='modal-body m-t-20'>
                                                    <h4 class='no-margin p-b-10'>".Lang::get('campaign.confirmStart')."</h4>
                                                    <button type='button' class='btn btn-danger pull-right m-l-5 m-r-5' data-dismiss='modal'>".Lang::get('campaign.cancelText')."</button>
                                                    <button type='button' class='btn btn-primary confirm-start-campaign pull-right m-l-5 m-r-5' data-id='".$campaign->id."'>".Lang::get('campaign.confirmTextAction')."</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-dialog -->";
                        }

                    }else if($campaign->status == 'waiting'){

                        return "<p><span style='color: darkgrey'><strong>".strtoupper($campaign->state)."</strong></span></p>
                                <button class='btn btn-success waiting disabled' title='Start this campaign'><i class='fa fa-play fs-14'></i></button>
                                <button class='btn btn-danger waiting disabled' title='Stop this campaign'><i class='fa fa-stop fs-14'></i></button>
                                <!-- Modal -->
                                <div class='modal fade slide-up disable-scroll' id='modalErrorCampaignWaiting'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content-wrapper'>
                                            <div class='modal-content'>
                                                <div class='modal-header clearfix text-left'>
                                                    <h3>".Lang::get('campaign.warningText')."</h3>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>
                                                      <i class='pg-close fs-14'></i>
                                                    </button>
                                                </div>
                                                <div class='modal-body m-t-20'>
                                                    <h4 class='no-margin p-b-10'>".Lang::get('campaign.waitingError')."</h4>
                                                    <button type='button' class='btn btn-info pull-right' data-dismiss='modal'>".Lang::get('campaign.doneText')."</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-dialog -->";

                    }else if($campaign->status == 'rejected'){

                        return "<p><span style='color: darkgray;'><strong>".strtoupper($campaign->state)."</strong></span></p>
                                <button class='btn btn-success rejected disabled' title='Start this campaign'><i class='fa fa-play fs-14'></i></button>
                                <button class='btn btn-danger rejected disabled' title='Stop this campaign'><i class='fa fa-stop fs-14'></i></button>
                                <!-- Modal -->
                                <div class='modal fade slide-up disable-scroll' id='modalErrorCampaignRejected'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content-wrapper'>
                                            <div class='modal-content'>
                                                <div class='modal-header clearfix text-left'>
                                                    <h3>".Lang::get('campaign.warningText')."</h3>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>
                                                      <i class='pg-close fs-14'></i>
                                                    </button>
                                                </div>
                                                <div class='modal-body m-t-20'>
                                                    <h4 class='no-margin p-b-10'>".Lang::get('campaign.rejectedError')."</h4>
                                                    <button type='button' class='btn btn-info pull-right' data-dismiss='modal'>".Lang::get('campaign.doneText')."</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-dialog -->";

                    }

                })
                ->addColumn('action', function($campaign){
                    return "<a href='".route('show.campaign', ['idCampaign' => $campaign->id])."' class='btn btn-info view-campaign' data-id='".$campaign->id."' title='View Campaign'><i class='fa fa-search'></i></a>
                            <a href='".route('edit.campaign', ['idCampaign' => $campaign->id])."' class='btn btn-primary edit-campaign' title='Edit Campaign'><i class='fa fa-edit'></i></a>
                            <a href='javascript:;' class='btn btn-danger delete-campaign' title='Delete Campaign' data-toggle='modal' data-target='#modalDelCampaign_".$campaign->id."'><i class='fa fa-trash'></i></a>
                            <!-- Modal -->
                                <div class='modal fade slide-up disable-scroll' id='modalDelCampaign_".$campaign->id."'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content-wrapper'>
                                            <div class='modal-content'>
                                                <div class='modal-header clearfix text-left'>
                                                    <h3>".Lang::get('campaign.warningText')."</h3>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>
                                                      <i class='pg-close fs-14'></i>
                                                    </button>
                                                </div>
                                                <div class='modal-body m-t-20'>
                                                    <h4 class='no-margin p-b-10'>".Lang::get('campaign.confirmDel')."</h4>
                                                    <button type='button' class='btn btn-danger pull-right m-l-5 m-r-5' data-dismiss='modal'>".Lang::get('campaign.cancelText')."</button>
                                                    <button type='button' class='btn btn-primary pull-right m-l-5 m-r-5 confirm-delete-campaign' data-id='".$campaign->id."'>".Lang::get('campaign.confirmTextAction')."</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-dialog -->";
                })
                ->rawColumns(['status', 'state', 'action'])
                ->make(true);

        }else{

            abort(403);

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function campaignStart(Request $request){

        if($request->isMethod('post')){

            $id = $request['id'];
            $campaign = Campaign::where('id', '=', $id)->first();

            $campaign->state = 'running';

            if($campaign->save()){
                return response()->json(array('success'=>true, 'msg'=> Lang::get('campaign.successStartNotif')));
            }else{
                return response()->json(array('success'=>false, 'msg'=> Lang::get('campaign.failStartNotif')));
            }

        }

        abort(403);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function campaignStop(Request $request){

        if($request->isMethod('post')){

            $id = $request['id'];
            $campaign = Campaign::where('id', '=', $id)->first();

            $campaign->state = 'stopped';

            if($campaign->save()){
                return response()->json(array('success'=>true, 'msg'=> Lang::get('campaign.successStopNotif')));
            }else{
                return response()->json(array('success'=>false, 'msg'=> Lang::get('campaign.failStopNotif')));
            }

        }

        abort(403);

    }

    /**
     * @param $idCampaign
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($idCampaign){

        $campaign = Campaign::where('id', '=', $idCampaign)->first();
        $user = Auth::user();

        if($campaign){

            if ($user->isAdmin()) {
                return view('campaigns.admin.adminShow', ['campaign' => $campaign]);
            } else {

                if ($campaign->user_id == $user->id) {
                    return view('campaigns.user.userShow', ['campaign' => $campaign]);
                }

                abort(403);

            }
        }

        abort(404);

    }

    /**
     * @param $idCampaign
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($idCampaign){

        $campaign = Campaign::where('id', '=', $idCampaign)->first();
        $user = Auth::user();

        if($campaign){

            if ($user->isAdmin()) {
                return view('campaigns.admin.adminEditForm', ['campaign' => $campaign]);
            } else {

                if ($campaign->user_id == $user->id) {
                    return view('campaigns.user.userEditForm', ['campaign' => $campaign]);
                }

                abort(403);

            }
        }

        abort(404);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function saveChanges(Request $request){

        $data = $request->all();
        $validator = $this->validatorEdit($data);

        if($validator->fails()){

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(isset($data['userId']) && $data['userId'] != 'self'){

            $user = User::where('name', '=', $data['userId'])->first();

        }else{

            $user = Auth::user();

        }

        $campaign = Campaign::where('id', '=', $data['idCampaign'])->first();

        $campaign->title          = $data['title'];
        $campaign->description    = $data['description'];

        if(Input::file('imgBack')){
            $imgBack = Input::file('imgBack');
            $filename = time() . '.' . $imgBack->getClientOriginalExtension();
            $path = public_path('/uploads/campaigns/'.$filename);
            Image::make($imgBack->getRealPAth())->save($path);
            $campaign->imgBackground = $filename;
        }
        else{
            if(empty($campaign->imgBackground)) $campaign->imgBackground = 'campaign_background_img.jpg';
        }

        if(Input::file('imgTag')){
            $imgTag = Input::file('imgTag');
            $filename = time() . '.' . $imgTag->getClientOriginalExtension();
            $path = public_path('/uploads/campaigns/'.$filename);
            Image::make($imgTag->getRealPAth())->save($path);
            $campaign->imgTag = $filename;
        }else{
            if(empty($campaign->imgTag)) $campaign->imgTag = 'campaign_tag_img.png';
        }

        if($campaign->save()){

            return redirect()->route('manage.campaign')->with('response', ['success'=>true, 'msg'=> Lang::get('campaign.successEditNotif')]);

        }
        else{

            return redirect()->back()->with('response', ['success'=>false, 'msg'=> Lang::get('campaign.failEditNotif')]);

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function campaignDelete(Request $request){

        if($request->isMethod('post')){

            $user = Auth::user();
            $id = $request['id'];
            $campaign = Campaign::where('id', '=', $id)->first();

            if($user->isAdmin()){

                if($campaign->delete()){
                    return response()->json(array('success'=>true, 'msg'=> Lang::get('campaign.successDeleteNotif')));
                }else{
                    return response()->json(array('success'=>false, 'msg'=> Lang::get('campaign.failDeleteNotif')));
                }

            }else{

                if($campaign->user_id == $user->id){

                    if($campaign->delete()){
                        return response()->json(array('success'=>true, 'msg'=> Lang::get('campaign.successDeleteNotif')));
                    }else{
                        return response()->json(array('success'=>false, 'msg'=> Lang::get('campaign.failDeleteNotif')));
                    }

                }

                return response()->json(array('success'=>false, 'msg'=> Lang::get('campaign.failDeleteNotif')));

            }

        }

        abort(403);

    }

}
