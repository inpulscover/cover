<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Campaign Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during campaign manage for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //TITLES
    'create'                        => 'Démarrer une campagne',
    'new'                           => 'Nouvelle Campagne',
    'show'                          => 'Details de la Campagne',
    'edit'                          => 'Modifier une Campagne',
    'manage'                        => 'Gestion des Campagnes',
    'mediaInfo'                     => 'Informations des médias',
    'basicInfo'                     => 'Informations Basiques',
    'userCampaign'                  => 'Utilisateurs',

    //LABELS
    'title'                         => 'Titre',
    'dateDebut'                     => 'Date Début',
    'dateFin'                       => 'Date Fin',
    'description'                   => 'Description',
    'imgTag'                        => 'Image de Tag',
    'imgBackground'                 => 'Image d\'Arrière Plan',

    //PLACEHOLDERS
    'ph_title'                      => 'Entrer un titre',
    'ph_date'                       => 'Sélectionner une date',
    'ph_description'                => 'Entrer une description',

    //VALIDATORS
    'titleRequired'                 => 'Le titre est requis',
    'titleMax'                      => 'La longueur maximale du titre est de 255 caractères',
    'dateDebutRequired'             => 'La date de début est requise',
    'dateDebutFormat'               => 'Le format de la date de début doit être MM/JJ/AAAA',
    'dateDebutMin'                  => 'La date de début doit être ultérieure ou égale à la date d\'aujourd\'hui',
    'dateFinRequired'               => 'La date de fin est requise',
    'dateFinFormat'                 => 'Le format de la date de fin doit être MM/JJ/AAAA',
    'dateFinMin'                    => 'La date de fin doit être ultérieure à la date de début',
    'imgBackRequired'               => 'L\'image d\'arrière plan est requise',
    'imgTagRequired'                => 'L\'image de tag est requise',
    'uploadFile'                    => 'Une erreur est survenue lors du chargement du fichier',
    'imageMIME'                     => 'Les fichiers chargés doivent être de type JPEG, JPG, BMP, ou PNG',
    'imageSize'                     => 'La taille des fichiers chargés doit être comprise entre 20 et 1024ko',
    'imageDimensions'               => 'Les dimensions de l\'image chargée doivent être de 1300 x 700 pixels',

    //ACTIONS TEXTS
    'createText'                    => 'Créer une nouvelle campagne',
    'saveText'                      => 'Sauvegarder',
    'editText'                      => 'Modifier',
    'confirmTextAction'             => 'Confirmer',
    'warningText'                   => 'Attention',
    'confirmText'                   => 'Confirmation',
    'cancelText'                    => 'Annuler',
    'doneText'                      => 'OK',

    //ACTIONS MESSAGES
    'confirmStop'                   => 'Êtes-vous sûr de vouloir arrêter cette campagne?',
    'confirmStart'                  => 'Confirmez-vous le début de cette campagne?',
    'confirmDel'                    => 'Êtes-vous sûr de vouloir supprimer cette campagne?',

    //ERRORS MESSAGES
    'waitingError'                  => 'Désolé, vous ne pouvez pas effectuer cette action car cette campagne n\'a pas encore été validée.',
    'rejectedError'                 => 'Désolé, vous ne pouvez pas effectuer cette action car cette campagne a été rejetée.',
    'noCampaign'                    => 'Désolé vous n\'avez aucune campagne.',

    //NOTIFICATIONS MESSAGES
    'successCreateNotif'            => 'Campagne créee avec succès !',
    'failCreateNotif'               => 'Une erreur est survenue lors de la création de la campagne !',
    'successEditNotif'              => 'Campagne modifiée avec succès !',
    'failEditNotif'                 => 'Une erreur est survenue lors de la modification de la campagne !',
    'successDeleteNotif'            => 'Campagne supprimée avec succès !',
    'failDeleteNotif'               => 'Une erreur est survenue lors de la suppression de la campagne !',
    'successStartNotif'             => 'Campagne démarrée avec succès !',
    'failStartNotif'                => 'Une erreur est survenue lors du démarrage de la campagne !',
    'successStopNotif'              => 'Campagne stoppée avec succès !',
    'failStopNotif'                 => 'Une erreur est survenue lors de l\'arrêt de la campagne !',

];