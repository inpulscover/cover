<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUser extends Model
{
    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'campaign_users';

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that mass assignable
     *
     * @var array
     */
    protected $fillable = ['firstName', 'lastName', 'sex', 'country', 'identifiant', 'imgProfile', 'provider_id'];

    // Campaigns_Users Providers
    public function providers(){

        return $this->belongsTo('App\Models\Provider');

    }

    //Campaigns_Users Campaigns
    public function campaigns(){

        return $this->belongsTo('App\Models\Campaign');

    }

}
