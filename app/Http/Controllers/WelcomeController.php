<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        if(Auth::check()){

            return redirect()->route('public.home');

        }

        return view('welcome');
    }

    public function home(){

        return redirect('/');

    }
}
