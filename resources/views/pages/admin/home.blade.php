@extends('layouts.backendAdmin')

@section('title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('page_linked_css')
    <link href="{{asset('css/app.6bb253d3befca4409bfe.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @include('panels.welcome-panel')

            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script src="{{asset('js/app.7c45f27865663b77adf8.js')}}" type="text/javascript"></script>
@endsection