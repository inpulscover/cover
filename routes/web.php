<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect' ]],
    function () {

        // Homepage Route
        Route::get('/', ['as'=>'welcome', 'uses'=>'WelcomeController@welcome']);
        Route::get('/home', ['as'=>'welcome', 'uses'=>'WelcomeController@home']);

        // Authentication Routes
        Auth::routes();

        // Public Routes
        Route::group(['middleware' => 'web'], function () {

            // Activation Routes
            Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

            Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
            Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
            Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

            // Socialite Register Routes
            Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
            Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

            // Route to for user to reactivate their user deleted account.
            Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
        });

        // Registered and Activated User Routes
        Route::group(['middleware' => ['auth', 'activated']], function () {

            // Activation Routes
            Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
            Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');

            //  Homepage Route - Redirect based on user role is in controller.
            Route::get('/dashboard', ['as' => 'public.home', 'uses' => 'UserController@index']);

            // Show users profile - viewable by other users.
            Route::get('profile/{username}', [
                'as' => '{username}',
                'uses' => 'ProfilesController@show',
            ]);
        });

        // Registered, activated, and is current user routes.
        Route::group(['middleware' => ['auth', 'activated', 'currentUser']], function () {

            // User Profile and Account Routes
            Route::resource(
                'profile',
                'ProfilesController', [
                    'only' => [
                        'show',
                        'edit',
                        'update',
                        'create',
                    ],
                ]
            );
            Route::put('profile/{username}/updateUserAccount', [
                'as' => '{username}',
                'uses' => 'ProfilesController@updateUserAccount',
            ]);
            Route::put('profile/{username}/updateUserPassword', [
                'as' => '{username}',
                'uses' => 'ProfilesController@updateUserPassword',
            ]);
            Route::delete('profile/{username}/deleteUserAccount', [
                'as' => '{username}',
                'uses' => 'ProfilesController@deleteUserAccount',
            ]);

            // Route to show user avatar
            Route::get('images/profile/{id}/avatar/{image}', [
                'uses' => 'ProfilesController@userProfileAvatar',
            ]);

            // Route to upload user avatar.
            Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);
        });

        // Registered, activated, and is admin routes.
        Route::group(['middleware' => ['auth', 'activated', 'role:admin']], function () {
            Route::resource('/users/deleted', 'SoftDeletesController', [
                'only' => [
                    'index', 'show', 'update', 'destroy',
                ],
            ]);

            Route::resource('users', 'UsersManagementController', [
                'names' => [
                    'index' => 'users',
                    'destroy' => 'user.destroy',
                ],
                'except' => [
                    'deleted',
                ],
            ]);

            Route::resource('themes', 'ThemesManagementController', [
                'names' => [
                    'index' => 'themes',
                    'destroy' => 'themes.destroy',
                ],
            ]);

            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
            Route::get('php', 'AdminDetailsController@listPHPInfo');
            Route::get('routes', 'AdminDetailsController@listRoutes');
        });

        //Campaign Routes
        Route::group(['middleware' => 'auth'], function (){
            Route::get('/campaign', ['as'=>'manage.campaign', 'uses'=>'CampaignController@manage']);
            Route::get('/campaign/manage', ['as'=>'manage.campaign', 'uses'=>'CampaignController@manage']);
            Route::get('/campaign/create', ['as'=>'create.campaign', 'uses'=>'CampaignController@create']);
            Route::post('/campaign/store', ['as'=>'save.campaign', 'uses'=>'CampaignController@save']);
            Route::get('/campaign/{idCampaign}/details', ['as'=>'show.campaign', 'uses'=>'CampaignController@show']);
            Route::get('/campaign/{idCampaign}/edit', ['as'=>'edit.campaign', 'uses'=>'CampaignController@edit']);
            Route::post('/campaign/storeEdit', ['as'=>'storeEdit.campaign', 'uses'=>'CampaignController@saveChanges']);


            /*USER DATATABLES URL REQUEST*/
            Route::get('/user/campaign-datatable', ['as'=>'userGetDatatable.campaign', 'uses'=>'CampaignController@getUserDataTableCampaign']);

            /*USER AJAX URL*/
            Route::post('campaign/start', ['as'=>'userStart.campaign', 'uses'=>'CampaignController@campaignStart']);
            Route::post('campaign/stop', ['as'=>'userStop.campaign', 'uses'=>'CampaignController@campaignStop']);
            Route::post('campaign/delete', ['as'=>'userDelete.campaign', 'uses'=>'CampaignController@campaignDelete']);
        });
    }
);