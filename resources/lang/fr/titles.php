<?php

return [

    'hello'            => 'Salut !',
    'regard'           => 'Cordialement,',
    'subText-1'        => 'Si vous rencontrez des difficultés pour cliquer sur le bouton ',
    'subText-2'        => ', copiez et collez l\'URL ci-dessous dans votre navigateur Web: ',

    'app'              => 'Laravel',
    'app2'             => 'Laravel Auth 2.0',
    'home'             => 'Accueil',
    'login'            => 'Connexion',
    'logout'           => 'Déconnexion',
    'register'         => 'Inscription',
    'contact'          => 'Contacts',
    'resetPword'       => 'Reinitialiser le mot de passe',
    'toggleNav'        => 'Activer/Désactiver la Navigation',
    'profile'          => 'Profil',
    'editProfile'      => 'Modifier le profil',
    'createProfile'    => 'Créer un profil',
    'welcome'          => 'Bienvenue ',

    'activation'       => 'Enregistrement en cours  | Activation requise',
    'exceeded'         => 'Erreur d\'activation',

    // MENU TITLES
    'dashboard'        => 'Tableau de Bord',
    'campaign'         => 'Campagne',

    'adminUserList'    => 'Administration des utilisateurs',
    'adminEditUsers'   => 'Modifier les utilisateurs',
    'adminNewUser'     => 'Créer un nouvel utilisateur',

    'adminThemesList' => 'Thèmes',
    'adminThemesAdd'  => 'Ajouter un nouveau thème',

    'adminLogs'        => 'Fichiers journaux',
    'adminPHP'         => 'Information PHP',
    'adminRoutes'      => 'Détails des routes',

    'allRight'         => 'Tous Droits Réservés.',

    //DATATABLES TITLES
    'dataTbTitle'      => 'Titre',
    'dataTbDesc'       => 'Description',
    'dataTbStatus'     => 'Statut',
    'dataTbState'      => 'Etat',
    'dataTbAction'     => 'Actions',

];
