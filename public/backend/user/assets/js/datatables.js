/* ============================================================
 * DataTables
 * Generate advanced tables with sorting, export options using
 * jQuery DataTables plugin
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
(function($) {

    'use strict';

    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    // Initialize datatable showing a search box at the top right corner
    var initTableWithSearch = function() {
        var table = $('#tableWithSearch');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    // Initialize datatable with ability to add rows dynamically
    var initTableWithDynamicRows = function() {
        var table = $('#tableWithDynamicRows');


        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };


        table.dataTable(settings);

        $('#show-modal').click(function() {
            $('#addNewAppModal').modal('show');
        });

        $('#add-app').click(function() {
            table.dataTable().fnAddData([
                $("#appName").val(),
                $("#appDescription").val(),
                $("#appPrice").val(),
                $("#appNotes").val()
            ]);
            $('#addNewAppModal').modal('hide');

        });
    }

    // Initialize datatable showing export options
    var initTableWithExportOptions = function() {
        var table = $('#tableWithExportOptions');


        var settings = {
            "sDom": "<'exportOptions'T><'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "oTableTools": {
                "sSwfPath": "assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "csv",
                    "sButtonText": "<i class='pg-grid'></i>",
                }, {
                    "sExtends": "xls",
                    "sButtonText": "<i class='fa fa-file-excel-o'></i>",
                }, {
                    "sExtends": "pdf",
                    "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
                }, {
                    "sExtends": "copy",
                    "sButtonText": "<i class='fa fa-copy'></i>",
                }]
            },
            fnDrawCallback: function(oSettings) {
                $('.export-options-container').append($('.exportOptions'));

                $('#ToolTables_tableWithExportOptions_0').tooltip({
                    title: 'Export as CSV',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_1').tooltip({
                    title: 'Export as Excel',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_2').tooltip({
                    title: 'Export as PDF',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_3').tooltip({
                    title: 'Copy data',
                    container: 'body'
                });
            }
        };


        table.dataTable(settings);

    }

    initTableWithSearch();
    initTableWithDynamicRows();
    initTableWithExportOptions();


    var tableCampaignUser = $('#userDataTableCampaign').dataTable({
        "sDom": "<'table-responsive't><'row'<p i>>",
        "sPaginationType": "bootstrap",
        "destroy": true,
        "scrollCollapse": true,
        processing: true,
        serverSide: true,
        ajax: '/user/campaign-datatable',
        columns: [
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'status', name: 'status'},
            {data: 'state', name: 'state'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
            "sEmptyTable": "<h3>Désolé, vous n'avez pas de campagne.</h3><h3>Sorry, you have no campaign.</h3>",
        },
        "iDisplayLength": 5
    })




    //DATATABLES BUTTONS ACTION

    $(document)
        .on('click', '.confirm-start-campaign', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:"/campaign/start",
                type:'post',
                data:{id:id},
                beforeSend:function(){
                    $('#modalStartCampaign_'+id).modal('hide');
                    $('.modal-backdrop').hide();
                    tableCampaignUser.block({
                        message: '<div class="progress-circle-indeterminate progress-circle-success"></div>',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                success: function (response) {

                    tableCampaignUser.unblock();
                    if(response.success==true)
                    {
                        $('.page-container').pgNotification({
                            style: 'flip',
                            message: response.msg,
                            position: 'top-right',
                            timeout: 0,
                            type: 'success'
                        }).show();

                        $('#userDataTableCampaign').DataTable().ajax.reload();

                        setTimeout(function () {
                            $('.pgn').remove();
                        }, 3000);

                    }else{
                        $('.page-container').pgNotification({
                            style: 'bar',
                            message: response.msg,
                            position: 'top',
                            timeout: 0,
                            type: 'danger'
                        }).show();

                        setTimeout(function () {
                            $('.pgn').remove();
                        }, 3000);
                    }
                }
            })
        })
        .on('click', '.confirm-stop-campaign', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:"/campaign/stop",
                type:'post',
                data:{id:id},
                beforeSend:function(){
                    $('#modalStopCampaign_'+id).modal('hide');
                    $('.modal-backdrop').hide();
                    tableCampaignUser.block({
                        message: '<div class="progress-circle-indeterminate progress-circle-success"></div>',
                        css: {
                            border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                        }
                    });
                },
                success: function (response) {

                    tableCampaignUser.unblock();
                    if(response.success==true)
                    {
                        $('.page-container').pgNotification({
                            style: 'flip',
                            message: response.msg,
                            position: 'top-right',
                            timeout: 0,
                            type: 'success'
                        }).show();

                        $('#userDataTableCampaign').DataTable().ajax.reload();

                        setTimeout(function () {
                            $('.pgn').remove();
                        }, 3000);

                    }else{
                        $('.page-container').pgNotification({
                            style: 'bar',
                            message: response.msg,
                            position: 'top',
                            timeout: 0,
                            type: 'danger'
                        }).show();

                        setTimeout(function () {
                            $('.pgn').remove();
                        }, 3000);
                    }
                }
            })

        })
        .on('click', '.waiting.disabled', function(e){
            e.preventDefault();
            $('#modalErrorCampaignWaiting').modal('show');
        })
        .on('click', '.rejected.disabled', function(e){
            e.preventDefault();
            $('#modalErrorCampaignRejected').modal('show');
        })
        .on('click', '.confirm-delete-campaign', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"/campaign/delete",
            type:'post',
            data:{id:id},
            beforeSend:function(){
                $('#modalDelCampaign_'+id).modal('hide');
                $('.modal-backdrop').hide();
                tableCampaignUser.block({
                    message: '<div class="progress-circle-indeterminate progress-circle-success"></div>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            },
            success: function (response) {

                tableCampaignUser.unblock();
                if(response.success==true)
                {
                    $('.page-container').pgNotification({
                        style: 'flip',
                        message: response.msg,
                        position: 'top-right',
                        timeout: 0,
                        type: 'success'
                    }).show();

                    $('#userDataTableCampaign').DataTable().ajax.reload();

                    setTimeout(function () {
                        $('.pgn').remove();
                    }, 3000);

                }else{
                    $('.page-container').pgNotification({
                        style: 'bar',
                        message: response.msg,
                        position: 'top',
                        timeout: 0,
                        type: 'danger'
                    }).show();

                    setTimeout(function () {
                        $('.pgn').remove();
                    }, 3000);
                }
            }
        })

    });

    setTimeout(function () {
        $('.pgn').remove();
    }, 3000);




})(window.jQuery);