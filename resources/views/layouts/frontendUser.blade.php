<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

    {{-- Fonts --}}
    @yield('template_linked_fonts')

    {{-- Styles --}}
    <link rel="apple-touch-icon" href="{{ asset('frontend/pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('frontend/pages/ico/76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('frontend/pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('frontend/pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- BEGIN PLUGINS -->
    <link href="{{ asset('frontend/assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend/assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend/assets/plugins/swiper/css/swiper.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGINS -->
    <!-- BEGIN PAGES CSS -->
    <link class="main-stylesheet" href="{{ asset('frontend/pages/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="{{ asset('frontend/pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css" />

    <!-- APP CSS >
    <link class="main-stylesheet" href="{{ asset('css/app.6bb253d3befca4409bfe.css') }}" rel="stylesheet" type="text/css" /-->

    @yield('page_linked_css')

    <style>
        input[type=checkbox]{
            width: 17px;
            height: 17px;
            position: absolute;
            left: 0px;
            top: 2px;
        }

        a:hover, a:focus, a:active{
            text-decoration: none !important;
        }
    </style>

</head>
<body class="pace-dark">
<!-- BEGIN HEADER -->
<nav class="header md-header light-solid" data-pages="header">
    <div class="container relative">
        <div class="pull-left">
            <div class="header-inner">
                <img src="{{ asset('frontend/assets/images/logo_black.png')}}" width="152" height="21" data-src-retina="{{ asset('frontend/assets/images/logo_black_2x.png')}}" class="logo" alt="logo">
            </div>
        </div>
        <!-- BEGIN HEADER TOGGLE FOR MOBILE & TABLET -->
        <div class="pull-right">
            <div class="header-inner">
                <div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10" data-pages="header-toggle" data-pages-element="#header">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
            </div>
        </div>
        <div class="pull-right menu-content clearfix" data-pages-direction="slideRight" id="header">
            <!-- BEGIN HEADER CLOSE TOGGLE FOR MOBILE -->
            <div class="pull-right">
                <a href="#" class="text-black link padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10" data-pages="header-toggle" data-pages-element="#header">
                    <i class=" pg-close_line"></i>
                </a>
            </div>
            <div class="p-t-10 p-b-10 clearfix m-b-5 hidden-xs hidden-sm">
                <div class="pull-right fs-12">
                    <a href="#" class="m-r-15 hint-text link text-black">Blog</a>
                    <a href="#" class="m-r-15 hint-text link text-black">Conditions d'utilisation</a>
                    <span class="m-r-15 text-black font-montserrat">+65 345 345 5555</span>
                </div>
            </div>
            <ul class="menu">
                <li>
                    <a class="active" href="{{ url('/') }}">{{Lang::get('titles.home')}}</a>
                </li>


                <li class="">
                    <a href="contact.html">{{Lang::get('titles.contact')}}</a>
                </li>
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">{{Lang::get('auth.login')}}</a></li>
                    <li><a href="{{ url('/register') }}">{{Lang::get('auth.register')}}</a></li>
                @else
                    <li class="classic">

                        <a href="javascript:;" data-text="Elements">
                            {{ Auth::user()->name }} <i class="pg-arrow_minimize m-l-5"></i>
                        </a>
                        <nav class="classic ">
                            <span class="arrow"></span>
                            <ul>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{Lang::get('titles.logout')}}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </nav>
                    </li>
                @endif
                <li class="classic">
                    <a href="javascript:;" data-text="Elements">
                        {{LaravelLocalization::getCurrentLocaleNative()}}  <i class="pg-arrow_minimize m-l-5"></i>
                    </a>
                    <nav class="classic ">
                        <span class="arrow"></span>
                        <ul>
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                    <a rel="alternate" hreflang="{{ $localeCode }}" href="/{{ $localeCode }}/">{{ $properties['native'] }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </li>
            </ul>
            <a class="btn btn-sm btn-bordered btn-primary block-title fs-12 hidden-sm hidden-xs" href="{{route('create.campaign')}}" data-text="Sign Up">{{Lang::get('campaign.create')}}</a>
        </div>
    </div>
</nav>

<!-- END HEADER -->

@yield('content')

<!-- BEGIN FOOTER -->
<section class=" p-t-40 bg-master-darker">
    <div class="container">

        <div class="row">
            <div class="col-sm-4">
                <img   src="{{asset('frontend/assets/images/logo_white.png')}}"  class="logo inline m-r-50" alt="">
                <div>
                    <ul class="no-style fs-11 no-padding font-arial">
                        <li class="inline no-padding"><a href="{{ url('/') }}" class=" text-master p-r-10 b-r b-grey">{{Lang::get('titles.home')}}</a></li>
                        <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 xs-no-padding xs-m-t-10">{{Lang::get('titles.contact')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 text-right font-arial sm-text-left">

                <p class="fs-12 hint-text p-t-10 text-white ">Copyright © <?php echo date('Y'); ?> INPULSION. {{Lang::get('titles.allRight')}}</p>
            </div>
        </div>
    </div>
</section>
<!-- END FOOTER -->
<!-- Scripts -->
<!-- BEGIN CORE FRAMEWORK -->
<script src="{{ asset('frontend/assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('frontend/pages/js/pages.image.loader.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/plugins/jquery/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- BEGIN SWIPER DEPENDENCIES -->
<script type="text/javascript" src="{{ asset('frontend/assets/plugins/swiper/js/swiper.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/plugins/velocity/velocity.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/plugins/velocity/velocity.ui.js') }}"></script>

<script type="text/javascript" src="{{ asset('frontend/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<!-- BEGIN RETINA IMAGE LOADER -->
<script type="text/javascript" src="{{ asset('frontend/assets/plugins/jquery-unveil/jquery.unveil.min.js') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN PAGES FRONTEND LIB -->
<script type="text/javascript" src="{{ asset('frontend/assets/js/contact.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/js/google_map.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/pages/js/pages.frontend.js') }}"></script>
<!-- END PAGES LIB -->
<!-- BEGIN YOUR CUSTOM JS -->
<script type="text/javascript" src="{{ asset('frontend/assets/js/custom.js') }}"></script>
<!-- END PAGES LIB -->

@yield('footer_scripts')

</body>
</html>
