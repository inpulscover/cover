@extends('layouts.frontendUser')

@section('title')
	{!! trans('titles.exceeded') !!}
@endsection

@section('content')
	<section class="p-b-65 p-t-90 full-height xs-full-width" style="background-color: #ddd;">
		<div class="container full-height">
			<div class="row full-height">
				<div class="container p-t-90">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="panel panel-danger">
								<div class="panel-heading">
									{!! trans('titles.exceeded') !!}
								</div>
								<div class="panel-body">
									<p>
										{!! trans('auth.tooManyEmails', ['email' => $email, 'hours' => $hours]) !!}
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection