<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){

            $user = Auth::user();
            $campaigns = Campaign::where('user_id', '=', $user->id);
            $nbC = $campaigns->count() or 0;

            if ($user->isAdmin()) {
                return view('pages.admin.home', ['nbC' => $nbC]);
            }

            return view('pages.user.home', ['nbC' => $nbC]);
        }

        return view('welcome');

    }
}
