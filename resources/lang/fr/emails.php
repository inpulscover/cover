<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Emails Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various emails that
    | we need to display to the user. You are free to modify these
    | language lines according to your application's requirements.
    |
    */

    /*
     * Activate new user account email.
     *
     */

    'activationSubject'  => 'Activation requise',
    'activationGreeting' => 'Bienvenue !',
    'activationMessage'  => 'Vous devez activer votre e-mail avant de pouvoir utiliser tous nos services.',
    'activationButton'   => 'Activer',
    'activationThanks'   => 'Merci d\'utiliser notre application!',

    /*
     * Goobye email.
     *
     */
    'goodbyeSubject'    => 'Désolé de vous voir partir...',
    'goodbyeGreeting'   => 'Salut :username,',
    'goodbyeMessage'    => 'Nous sommes vraiment désolés de vous voir partir. Nous voulions vous informer que votre compte a été supprimé. Merci pour le temps que nous avons partagé. Vous avez '.config('settings.restoreUserCutoff').' jours pour restaurer votre compte.',
    'goodbyeButton'     => 'Restaurer votre compte',
    'goodbyeThanks'     => 'Nous espérons vous revoir bientôt!',

];
