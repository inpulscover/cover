<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {

            $table->engine = 'MyISAM';

            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->date('dateDebut');
            $table->date('dateFin');
            $table->string('imgTag');
            $table->string('imgBackground');
            $table->string('code');
            $table->string('link')->nullable();
            $table->string('status')->default('draft');
            $table->string('state')->default('undefined');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
