@extends('layouts.frontendUser')

@section('title')
    {{Lang::get('titles.login')}}
@endsection

@section('page_linked_css')

    <style>
        .form-horizontal .form-group .control-label{
            opacity: 1;
        }

        @media(max-width: 991px){
            .xs-m-l-15{
                margin-left: 15px;
            }
        }
    </style>
@endsection

@section('content')
    <section class="p-b-65 p-t-90 full-height xs-full-width" style="background-color: #ddd;">
        <div class="container full-height">
            <div class="row full-height">
                <div class="container p-t-80">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3>{{Lang::get('titles.login')}}</h3></div>
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">{{Lang::get('auth.email')}}</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" placeholder="{{Lang::get('auth.ph_email')}}" value="{{ old('email') }}" required autofocus>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">{{Lang::get('auth.password')}}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control" name="password" placeholder="{{Lang::get('auth.ph_password')}}" required>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4 xs-m-l-15">
                                                <div class="">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span style="line-height: 30px; padding-left: 10px;">{{Lang::get('auth.rememberMe')}}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin-bottom-3">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{Lang::get('auth.login')}}
                                                </button>

                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{Lang::get('auth.forgot')}}
                                                </a>
                                            </div>
                                        </div>

                                        @if(config('settings.socialLoginStatus'))
                                            <p class="text-center margin-bottom-3">
                                                Or Login with
                                            </p>

                                            @include('partials.socials-icons')
                                        @endif

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
