<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Campaign Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during campaign manage for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //TITLES
    'create'                        => 'Start a Campaign',
    'new'                           => 'New Campaign',
    'show'                          => 'Campaign Details',
    'edit'                          => 'Edit Campaign',
    'manage'                        => 'Campaigns Management',
    'mediaInfo'                     => 'Medias Informations',
    'basicInfo'                     => 'Basics Informations',
    'userCampaign'                  => 'Campaign Users',

    //LABELS
    'title'                         => 'Title',
    'dateDebut'                     => 'Begin Date',
    'dateFin'                       => 'End Date',
    'description'                   => 'Description',
    'imgTag'                        => 'Tag Image',
    'imgBackground'                 => 'Background Image',

    //PLACEHOLDERS
    'ph_title'                      => 'Enter a title',
    'ph_date'                       => 'Pick a Date',
    'ph_description'                => 'Enter a description',

    //VALIDATORS
    'titleRequired'                 => 'Title is Required',
    'titleMax'                      => 'Title maximum length is 255 characters',
    'dateDebutRequired'             => 'Date Begin Campaign is required',
    'dateDebutFormat'               => 'Date Begin Campaign format must be m/d/Y',
    'dateDebutMin'                  => 'Date Begin Campaign must be later than or equal to today\'s date',
    'dateFinRequired'               => 'Date End Campaign is required',
    'dateFinFormat'                 => 'Date End Campaign format must be m/d/Y',
    'dateFinMin'                    => 'Date End Campaign must be later than Date Begin Campaign',
    'imgBackRequired'               => 'The Background image is required',
    'imgTagRequired'                => 'The Tag image is required',
    'uploadFile'                    => 'An error occurred while uploading file',
    'imageMIME'                     => 'The uploaded files must be of type JPEG, JPG, BMP, or PNG',
    'imageSize'                     => 'The uploaded files size must be between 20 and 1024ko',
    'imageDimensions'               => 'The dimensions of the uploaded image must be 1300x700 pixel',

    //ACTIONS TEXTS
    'createText'                    => 'Create a new campaign',
    'saveText'                      => 'Save changes',
    'editText'                      => 'Edit',
    'confirmTextAction'             => 'Confirm',
    'warningText'                   => 'Warning',
    'confirmText'                   => 'Confirmation',
    'cancelText'                    => 'Cancel',
    'doneText'                      => 'OK',

    //ACTIONS MESSAGES
    'confirmStop'                   => 'Are you sure you want to stop this campaign?',
    'confirmStart'                  => 'Do you confirm the start of this campaign?',
    'confirmDel'                    => 'Are you sure you want to delete this campaign?',

    //ERRORS MESSAGES
    'waitingError'                  => 'Sorry, you can not perform this action because this campaign has not yet been validated.',
    'rejectedError'                 => 'Sorry, you can not perform this action because this campaign was rejected.',
    'noCampaign'                    => 'Sorry you have no campaign.',

    //NOTIFICATIONS MESSAGES
    'successCreateNotif'            => 'Campaign created successfully !',
    'failCreateNotif'               => 'An error occurred while creating campaign !',
    'successEditNotif'              => 'Campaign edited successfully !',
    'failEditNotif'                 => 'An error occurred while editing campaign !',
    'successDeleteNotif'            => 'Campaign deleted successfully !',
    'failDeleteNotif'               => 'An error occurred while deleting campaign !',
    'successStartNotif'             => 'Campaign started successfully !',
    'failStartNotif'                => 'An error occurred while starting campaign !',
    'successStopNotif'              => 'Campaign stopped successfully !',
    'failStopNotif'                 => 'An error occurred while stopping campaign !'

];