@extends('layouts.backendUser')

@section('title')
    {{Lang::get('titles.dashboard')}}
@endsection

@section('page_linked_css')
    <link href="{{asset('css/app.6bb253d3befca4409bfe.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12">
                    <!-- START WIDGET D3 widget_graphWidget-->
                    <div class="widget-12 card no-border widget-loader-circle no-margin">
                        <div class="card-header">
                            <h2>{{strtoupper(Lang::get('titles.welcome')). Auth::user()->name}}</h2>
                        </div>
                        <div class="card-block">
                            <h2 class="lead">
                                {{ trans('auth.loggedIn') }}
                            </h2>
                            <div class="col-lg-3 m-b-10">
                                <!-- START WIDGET widget_statTile-->
                                <div class="card no-border bg-warning">
                                    <div class="card-header">
                                        <h4 class="bold">{{Lang::get('titles.campaign')}}</h4>
                                    </div>
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="no-margin p-b-5 text-danger bold">{{$nbC}}</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET -->
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET -->
                </div>
            </div>
        </div>
    </div>
    <!--div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @include('panels.welcome-panel')

            </div>
        </div>
    </div-->

@endsection

@section('footer_scripts')
    <script src="{{asset('js/app.7c45f27865663b77adf8.js')}}" type="text/javascript"></script>
@endsection