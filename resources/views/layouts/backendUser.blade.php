<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

    {{-- Fonts --}}
    @yield('template_linked_fonts')

    {{-- Styles --}}
    <link rel="apple-touch-icon" href="{{asset('backend/user/pages/ico/60.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('backend/user/pages/ico/76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('backend/user/pages/ico/120.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('backend/user/pages/ico/152.png')}}">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <!-- BEGIN PLUGINS -->
    <link href="{{asset('backend/user/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/user/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/user/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/user/assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/user/assets/plugins/mapplic/css/mapplic.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/rickshaw/rickshaw.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('backend/user/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('backend/user/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/user/assets/css/dashboard.widgets.css')}}" rel="stylesheet" type="text/css')}}" media="screen" />
    <link href="{{asset('backend/user/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{asset('backend/user/pages/css/themes/modern.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/user/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('backend/user/assets/plugins/summernote/css/summernote.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link type="text/css" rel="stylesheet" href="{{asset('backend/user/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    @yield('page_linked_css')

    <style type="text/css">

        @if (Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0))
            .user-avatar-nav {
            background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
            background-size: auto 100%;
        }
        @endif

        .table tbody tr td{
            vertical-align: middle;
        }

        .btn-tag {
            cursor: default;
        }

        a:hover, a:focus, a:active{
            text-decoration: none !important;
        }

    </style>

</head>
<body class="fixed-header horizontal-menu horizontal-app-menu dashboard">
<!-- START HEADER -->
    @include('../partials/user.nav-user')
<!-- END HEADER -->

<!-- START PAGE-CONTAINER -->
<div class="page-container ">
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
            <!-- START CONTAINER FLUID -->
            <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

                @yield('content')

            </div>
            <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        <div class=" container   container-fixed-lg footer">
            <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text">Copyright &copy; 2017 </span>
                    <span class="font-montserrat">REVOX</span>.
                    <span class="hint-text">All rights reserved. </span>
                    <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> <span class="muted">|</span> <a href="#" class="m-l-10">Privacy Policy</a></span>
                </p>
                <p class="small no-margin pull-right sm-pull-reset">
                    Hand-crafted <span class="hint-text">&amp; made with Love</span>
                </p>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- END COPYRIGHT -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <div class="pgn-wrapper" data-position="top-right" style="top: 95px; right: 0px;">
        @if(Session::has('response') && Session::get('response.success') == true)
            <div class="pgn push-on-sidebar-open pgn-flip">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <span>{{Session::get('response.msg')}}</span>
                </div>
            </div>
        @endif
    </div>
    <div class="pgn-wrapper text-center" data-position="top" style="top: 95px;">
        @if(Session::has('response') && Session::get('response.success') == false)
        <div class="pgn push-on-sidebar-open pgn-bar">
            <div class="alert alert-danger">
                <div class="container">
                    <span>{{ Session::get('response.msg') }}</span>
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- END PAGE CONTAINER -->

@include('../partials/user.quickview-user')

@include('../partials/user.search-user')

<!-- BEGIN VENDOR JS -->
<script src="{{asset('js/app.7c45f27865663b77adf8.js')}}" type="text/javascript"></script>

<script src="{{asset('backend/user/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/tether/js/tether.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/user/assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/user/assets/plugins/classie/classie.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/lib/d3.v3.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/nv.d3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/src/utils.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/src/tooltip.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/src/interactiveLayer.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/src/models/axis.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/src/models/line.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/nvd3/src/models/lineWithFocusChart.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/mapplic/js/hammer.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/mapplic/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/mapplic/js/mapplic.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-metrojs/MetroJs.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/skycons/skycons.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('backend/user/assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/user/assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
<script src="{{asset('backend/user/assets/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/js/form_elements.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{asset('backend/user/pages/js/pages.min.js')}}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{asset('backend/user/assets/js/dashboard.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/js/notifications.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/user/assets/js/scripts.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->

@yield('footer_scripts')

</body>
</html>