<?php

return [

    'hello'            => 'Hello !',
    'regard'           => 'Regards,',
    'subText-1'        => 'If you’re having trouble clicking the ',
    'subText-2'        => ' button, copy and paste the URL below into your web browser: ',

    'app'              => 'Laravel',
    'app2'             => 'Laravel Auth 2.0',
    'home'             => 'Home',
    'login'            => 'Login',
    'logout'           => 'Logout',
    'register'         => 'Register',
    'contact'          => 'Contacts',
    'resetPword'       => 'Reset Password',
    'toggleNav'        => 'Toggle Navigation',
    'profile'          => 'Profile',
    'editProfile'      => 'Edit Profile',
    'createProfile'    => 'Create Profile',
    'welcome'          => 'Welcome ',

    'activation'       => 'Registration Started  | Activation Required',
    'exceeded'         => 'Activation Error',

    //MENU TITLES
    'dashboard'        => 'Dashboard',
    'campaign'         => 'Campaign',

    'adminUserList'    => 'Users Administration',
    'adminEditUsers'   => 'Edit Users',
    'adminNewUser'     => 'Create New User',

    'adminThemesList'  => 'Themes',
    'adminThemesAdd'   => 'Add New Theme',

    'adminLogs'        => 'Log Files',
    'adminPHP'         => 'PHP Information',
    'adminRoutes'      => 'Routing Details',

    'allRight'         => 'All Rights Reserved.',

    //DATATABLES TITLES
    'dataTbTitle'      => 'Title',
    'dataTbDesc'       => 'Description',
    'dataTbStatus'     => 'Status',
    'dataTbState'      => 'State',
    'dataTbAction'     => 'Actions',

];
