<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'providers';

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that mass assignable
     *
     * @var array
     */
    protected $fillable = ['libelle', 'description'];

    // Providers Campaigns_Users
    public function campaignsUsers(){

        return $this->hasMany('App\Models\CampaignUser');

    }
}
